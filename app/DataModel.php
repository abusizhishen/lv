<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataModel extends Model
{
    protected $table = 'test';
    protected $guarded = ['updated_at','created_at'];
    public $timestamps = False;
}
