<?php

namespace App\Http\Controllers;

use App\DataModel;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('index');
    }

    public function img_upload(Request $request)
    {
        if ($request->hasFile('avator')){
            $request->avator->store('images');
            dd('file upload success');
        }else{
            dd('file upload failed');
        }
    }

    public function insert()
    {
        print_r('开始插入');
        $data = new DataModel;
        $string = '1234567890qwertyuiop[]asdfghjkl;"\/.,mnbvcxz!@#$%^&*()_+?><~';
        $count = 1;

        while ($count<=10){
            $data->content = str_shuffle($string);
            $data->save();
            $count++;
            print_r('当前插入行数'.$count);
        }

        print_r('插入结束');


    }
}
