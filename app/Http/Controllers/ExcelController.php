<?php

namespace App\Http\Controllers;

use AetherUpload\AetherUploadServiceProvider;
use AetherUpload\ResourceHandler;
use App\DataModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function generate()
    {
        $writer = new \XLSXWriter();
        //文件名
        $filename = "example.xlsx";
        //设置 header，用于浏览器下载
        header('Content-disposition: attachment; filename="'.\XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $data_array = DataModel::take(50000)->get()->toArray();
        $data = ['这是描述的啦啦啦','dddddddefcwqdw','小额服务费为东风的味道','反反复复付付付付若翁付无','的体格呃呃呃鹅鹅鹅鹅鹅鹅饿','的午饭晚饭11356'];

        //每列的标题头
        $title = array (
            0 => 'ID',
            1 => '会员昵称',
            2 => '会员编号',
            3 => '手机号码',
            4 => '会员昵称',
            5 => '邮箱',
            6 => '积分',
            7=>  'content'
        );
        //工作簿名称
        $sheet1 = 'sheet1';

        //对每列指定数据类型，对应单元格的数据类型
        foreach ($title as $key => $item){
            $col_style[] = $key ==5 ? 'price': 'string';
        }

        //设置列格式，suppress_row: 去掉会多出一行数据；widths: 指定每列宽度
        $writer->writeSheetHeader($sheet1, $col_style, ['suppress_row'=>true,'widths'=>[20,20,20,20,20,20,20,20]] );
        //写入第二行的数据，顺便指定样式
        $writer->writeSheetRow($sheet1, ['member导出Excel'],
            ['height'=>32,'font-size'=>20,'font-style'=>'bold','halign'=>'center','valign'=>'center']);

        /*设置标题头，指定样式*/
        $styles1 = array( 'font'=>'宋体','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee',
            'halign'=>'center', 'border'=>'left,right,top,bottom');
        $writer->writeSheetRow($sheet1, $title, $styles1);

        $styles2 = ['height'=>16];

        foreach($data_array as $row){
            $writer->writeSheetRow($sheet1, $row+$data,$styles2);
        }

        //合并单元格，第一行的大标题需要合并单元格
        $writer->markMergedCell($sheet1, $start_row=0, $start_col=0, $end_row=0, $end_col=6);
        //输出文档
        $writer->writeToStdOut();
        exit(0);
    }

    public function reader()
    {
        $file_path = 'd:\www\laravel\1.xlsx';
        $reader = \PHPExcel_IOFactory::load($file_path);
        // 读取规则
        $sheet_read_arr = array();
        $sheet_read_arr["sheet1"] = array("A","B","C","D","F");
        $sheet_read_arr["sheet2"] = array("A","B","C","D","F");

    // 循环所有的页
        foreach ($sheet_read_arr as $key => $val)
        {
            $currentSheet = $reader->getSheetByName($key);// 通过页名称取得当前页
            $row_num = $currentSheet->getHighestRow();// 当前页行数

            // 循环从第二行开始，第一行往往是表头
            for ($i = 2; $i <= $row_num; $i++) {
                $cell_values = array();
                foreach ($val as $cell_val) {
                    $address = $cell_val . $i;// 单元格坐标

                    // 读取单元格内容
                    $cell_values[] = $currentSheet->getCell($address)->getFormattedValue();
                }

                // 看看数据
                return $cell_values;
            }
        }
    }
}

class PHPExcelReadFilter implements \PHPExcel_Reader_IReadFilter {
    public $endRow = 10000;

    public function readCell($column, $row, $worksheetName = '') {
        //如果endRow没有设置表示读取全部
        if (!$this->endRow) {
            return true;
        }
        //只读取指定的行
        if ($row >= $this->startRow && $row <= $this->endRow) {
            return true;
        }

        return false;
    }
}

function readFromExcel($excelFile, $excelType = null, $startRow = 1, $endRow = null) {

    $excelReader = \PHPExcel_IOFactory::createReader("Excel2007");
    $excelReader->setReadDataOnly(true);

    //如果有指定行数，则设置过滤器
    if ($startRow && $endRow) {
        $perf           = new PHPExcelReadFilter();
        $perf->startRow = $startRow;
        $perf->endRow   = $endRow;
        $excelReader->setReadFilter($perf);
    }

    $phpexcel    = $excelReader->load($excelFile);
    $activeSheet = $phpexcel->getActiveSheet();
    if (!$endRow) {
        $endRow = $activeSheet->getHighestRow(); //总行数
    }

    $highestColumn      = $activeSheet->getHighestColumn(); //最后列数所对应的字母，例如第2行就是B
    $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); //总列数

    $data = array();
    for ($row = $startRow; $row <= $endRow; $row++) {
        for ($col = 0; $col < $highestColumnIndex; $col++) {
            $data[$row][] = (string) $activeSheet->getCellByColumnAndRow($col, $row)->getValue();
        }
    }
    return $data;

    AetherUploadServiceProvider::class;
    ResourceHandler::class;
}