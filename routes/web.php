<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){
    return view('index');
});
Route::get('/insert','Controller@insert');
Route::get('excel', 'ExcelController@generate');
Route::get('reader', 'ExcelController@reader');


Route::get('upload','UploadController@index');
Route::post('upload/upload', 'UploadController@upload');

\Illuminate\View\FileViewFinder::class;